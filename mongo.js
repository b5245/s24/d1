// Inclusion
/*
	- allows to include/add specific fields only when retrieving documents

	- the value provided is 1 to denote that the field is being included

	Syntax:
*/
db.users.find(
	{
		firstName: "Jane"
	},
	{
		firstName: 1,
		lastName: 1,
		contact: 1
	}
)

// Exclusion
/*
- the value provided is 0 to denote that the field is being excluded
*/

db.users.find(
	{
		firstName: "Jane"
	},
	{

		contact: 0,
		department: 0
	}
)

// Suppressing the ID field
/*
	- Allows to excluede the "_ID" field when retrieving documents/
	-when using field projection, field inclusion and exclusion may not be used at the same time

	-excluding the "_id" field is the only exception in this rule.

*/

db.users.find(
	{
		firstName: "Jane"
	},
	{
		firstName: 1,
		lastName: 1,mountain bike
		contact: 1,
		_id: 0
	}
)


// Returning Specific Fields in Embedded Documents

db.users.find(
{
	firstName: "Jane"
},
{
	firstName: 1,
	lastName: 1,
	"contact.phone": 1
}
)

// Supressing Specific Fields in embedded documents

db.users.find(
{
	firstName: "Jane"
},
{
	"contact.phone": 0
}
);

/*
Project Specific Array Elements in the Returned  Array
The $slice operator allows us to retrieve only one element that matches the search criteria
*/

db.users.find(
	{ "namearr": [
		{
			namea: "Jane"
		}
	]
},
	{ "namearr":
		{$slice: 1}
	}
)

// $regex operator (regular expression)
/*
	- allows us to find documents that match a specific
	- string pattern using regular expressions

	SYntax
	db.users.find({ field: $regex: 'pattern', $options:'$optioValue'});


*/

db.users.find({ firstName: {$regex: 'N' } }).pretty();

// Case sensitive query
db.users.find({ firstName: { $regex: 'N' } });
// Case insensitive query
db.users.find({ firstName: { $regex: 'j', $options: '$i' } });

db.users.find ({
	$or:[
		{ firstName: { $regex: 's', $options: '$i' } },
		{ lastName: { $regex: 'd', $options: '$i'} }
	]
}, 

{ 	firstName: 1,
	lastName: 1,
	_id: 0
}).pretty();